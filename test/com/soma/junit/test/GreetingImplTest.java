package com.soma.junit.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.soma.junit.hello.GreetingImpl;
import com.soma.junit.hello.Greetings;

public class GreetingImplTest {

	@Test
	public void greetShouldReturnParameterName() {
		Greetings greetings = new GreetingImpl();
		String expectedResult = greetings.great("Mahendra");
		assertNotNull(expectedResult);
		assertEquals("Hello, Mahendra", expectedResult);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void greetShouldThrowExceptionFor_NameIsNull() {
		Greetings greeting = new GreetingImpl();
		greeting.great(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void greetShouldThrowExceptionFor_NameIsEmpty() {
		Greetings greeting = new GreetingImpl();
		greeting.great("");
	}

}
