package com.soma.junit.hello;

public class GreetingImpl implements Greetings {

	@Override
	public String great(String word) {
		if(word == null || word.length() == 0)
			throw new IllegalArgumentException();
		return "Hello, " + word;
	}

}
